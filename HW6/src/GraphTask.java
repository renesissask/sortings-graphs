
import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main ( String[] args ) {
        GraphTask a = new GraphTask();
        a.run(3);
//        a.checkTime(5);

    }


    public void run ( int n ) {

        Graph g = new Graph("hi");
        g.createRandomSimpleGraph(n);
//        g.createRandomTree(n);
        System.out.println(g);
        System.out.println(String.format("Contains cycles: %s", g.isContainingCycles()));
        System.out.println(String.format("Kahn order: %s", Arrays.toString(g.topologicalSortKahn())));
        System.out.println(String.format("dfs order: %s", Arrays.toString(g.dfsTopsort())));
        System.out.println(String.format("Find All strongly connected componenets: %s", g.getStronglyConnectedComponents()));
        System.out.println("");
        g.findHamiltonianPath();
    }


    public void checkTime ( int n ) {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(n);
        System.out.println(String.format("Test only for sort with premade graph with %s vertices and %s arcs", n, g.arcCount));

        long sTime = System.nanoTime();
        g.topologicalSortKahn();
        long fTime = System.nanoTime();
        long diff = fTime - sTime;
        System.out.println("Kahn Sort time(ns)  : " + diff);
        System.out.println("Kahn Sort time(ms)  : " + diff / 1000000);

        sTime = System.nanoTime();
        g.dfsTopsort();
        fTime = System.nanoTime();
        diff = fTime - sTime;
        System.out.println("\nDFS Sort time(ns)  : " + diff);
        System.out.println("DFS Sort time(ms)  : " + diff / 1000000);

        sTime = System.nanoTime();
        g.findHamiltonianPath();
        fTime = System.nanoTime();
        diff = fTime - sTime;
        System.out.println("\nFind hamilton path first elem (ns)  : " + diff);
        System.out.println("Find hamilton path first elem (ms)  : " + diff / 1000000);

        sTime = System.nanoTime();
        g.getStronglyConnectedComponents();
        fTime = System.nanoTime();
        diff = fTime - sTime;
        System.out.println("\nFinds strongly connected components (ns)  : " + diff);
        System.out.println("Finds strongly connected components (ms)  : " + diff / 1000000);
    }


    /**
     * Representation of a Vertex.
     *
     * @author Enricqe Rene Sissask
     * @author Jaanus Pöial
     */
    public class Vertex {

        private final String id;
        private Vertex next;
        private Arc first;
        private int inDegrees = 0;
        private int count;
        private int info = 0;
        private boolean checked = false;
        private boolean visited = false;


        /**
         * Creates a Vertex with it's id, pointer to the next vertex and with a pointer to its Arc
         *
         * @param s name of the Vertex
         * @param v pointer to the next Vertex
         * @param e Pointer to the arc, which shows the target Vertex of the this vertex
         */
        Vertex ( String s, Vertex v, Arc e ) {
            id = s;
            next = v;
            first = e;
        }

        /**
         * Creates a Vertex without any pointers to the next Vertex or first Arc
         *
         * @param s name of the Vertex
         */
        Vertex ( String s ) {
            this(s, null, null);
        }

        /**
         * Returns a string representation of this Vertex's name
         */
        @Override
        public String toString () {
            return id;
        }


        /**
         * Counts all current vertex connections (arcs).
         *
         * @return number of connections
         */
        public int countAllConnections() {
            if (!this.hasConnection()) return 0;
            int count = 1;
            Arc current = this.first;
            while (current.hasNextConnection()) {
                count++;
                current = current.next;
            }
            return count;
        }

        /**
         * Checks if current vertex have unvisited connections
         *
         * @return {@code true} if current vertex have connection
         * which is not used. {@code false} otherwise.
         */
        public boolean hasUnvisitedConnections() {
            Arc current = first;
            while (current != null) {
                if (!current.target.isVisited()) return true;
                current = current.next;
            }
            return false;
        }

        /**
         * Finds and gives vertex unused connections.
         * Before usage is recommended to check for this type connections,
         * to avoid RuntimeException.
         *
         * @return {@code Arc} if unused connection is present.
         * @throws RuntimeException if unused connections is not present.
         *                          All {@code arc.target.isVisited = false}.
         */
        public Arc getUnvisitedConnections() {
            Arc current = first;
            while (current != null) {
                if (!current.target.isVisited()) return current;
                current = current.next;
            }
            throw new RuntimeException("No unused connections! Vertex id:" + id);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Vertex)) return false;
            Vertex vertex = (Vertex) o;
            return info == vertex.info &&
                    visited == vertex.visited &&
                    Objects.equals(id, vertex.id) &&
                    Objects.equals(next, vertex.next) &&
                    Objects.equals(first, vertex.first);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, next, first, info);
        }


        /**
         * Checks if current node has next (sibling) node.
         *
         * @return {@code true} if current node has next node.
         * {@code this.nextVertex != null}
         * {@code false} otherwise,
         */
        public boolean hasNextNode() {
            return next != null;
        }

        /**
         * Checks if current node has connection (arc).
         *
         * @return {@code true} if connection is present.
         * {@code arc != null}.
         * {@code false} otherwise if connection is not present.
         */
        public boolean hasConnection() {
            return first != null;
        }

        /**
         * Checks if current vertex was visited.
         *
         * @return {@code true} if node was not visited
         * {@code false} otherwise
         */
        public boolean isVisited() {
            return visited;
        }

        /**
         * Set vertex param visited to given value.
         *
         * @param visited value {@code boolean}
         */
        public void setVisited(boolean visited) {
            this.visited = visited;
        }

        /**
         * Set vertex next vertex to given value.
         *
         * @param vertex next vertex. {@code Vertex}
         */
        public void setNext(Vertex vertex) {
            next = vertex;
        }
    }


    /**
     * Represents an Arc.
     *
     * @author Enricqe Rene Sissask
     * @author Jaanus Pöial
     */
    public class Arc {

        private final String id;
        private Vertex target;
        private Arc next;
        private boolean checked = false;

        /**
         * Creates an Arc with it's id, pointer to the target vertex and with a pointer to the next Arc
         *
         * @param s name of the Arc.
         * @param v Destination's pointer to the target Vertex
         * @param a Pointer to the next Arc in the graph
         */
        Arc ( String s, Vertex v, Arc a ) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Creates a Vertex with its id without any pointers
         *
         * @param s name of the Arc.
         */
        Arc ( String s ) {
            this(s, null, null);
        }

        /**
         * Converts the Arc into a string
         *
         * @return the string representation of this Arc's id
         */
        @Override
        public String toString () {
            return id;
        }

        /**
         * Get vertices last connection - arc chains last arc.
         *
         * @return {@code Arc} last arc in chain.
         * {@code this} if no next arc. {@code next == null}
         */
        public Arc getLastConnection() {
            if (this.next == null) return this;
            Arc current = next;
            while (current.next != null) {
                current = current.next;
            }
            return current;
        }

        /**
         * Checks if current arc has nex arc.
         *
         * @return {@code true} if current arc is not null.
         * {@code next != null}
         */
        public boolean hasNextConnection() {
            return next != null;
        }

    }


    /**
     * Represents a graph
     *
     * @author Enricqe Rene Sissask
     * @author Jaanus Pöial
     */
    public class Graph {


        private final String id;
        private Vertex first;
        private int vertexCount = 0;
        private int arcCount = 0;

        /**|
         * Creates a Graph with its id and pointer to the first Vertex of the graph
         *
         * @param s name of the Graph.
         * @param v Pointer to the first Vertex
         */
        Graph ( String s, Vertex v ) {
            id = s;
            first = v;
        }

        /**
         * Creates a Graph with its id and without any pointers
         *
         * @param s name of the Graph.
         */
        Graph ( String s ) {
            this(s, null);
        }

        /**
         * Converts the graph into a string
         *
         * @return a string representation of this Graph
         * "v0 --> av0_v3 (v0->v3)"
         */
        @Override
        public String toString () {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while ( v != null ) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while ( a != null ) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.id);
                    sb.append("->");
                    sb.append(a.target.id);
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }


        /**
         * Creates a new vertex and adds it as a first Vertex and shifts other Vertexes by 1 index
         *
         * @param vid id of the Vertex which will be created
         * @return The newly made Vertex
         */
        public Vertex createVertex ( String vid ) {
            vertexCount++;
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }


        /**
         * Creates a new Arc which connects 2 Vertexes togethor.
         *
         * @param aid  Name of the arc
         * @param from Pointer to the destination Vertex
         * @param to   Pointer to the target Vertex
         * @return The newly made Arc
         */
        public Arc createArc ( String aid, Vertex from, Vertex to ) {
            validateIfVertexIsConnectedToGraph(this, from.id);
            validateIfVertexIsConnectedToGraph(this, to.id);
            Arc res = new Arc(aid);
            arcCount++;
            to.inDegrees++;
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }
        public boolean isContainingCycles() {
            List<Set<Vertex>> sccList = getStronglyConnectedComponents();
            reset();
            for (Set<Vertex> set : sccList) {
                if (set.size() > 1) return true;
            }
            return false;
        }

        /**
         * Finds all strongly connected components.
         * <p>
         * Steps:
         * 1. Get Dfs of current Graph.
         * 2. Reverse graph.
         * 3. Make dfs on reversed Graph with previously made Dfs order.
         * Every group will be strongly connected components.
         *
         * @return {@code List<Set<Vertex>>} list with sets of strongly connected components
         */
        public List<Set<Vertex>> getStronglyConnectedComponents() {
//            Instant start = Instant.now();
            if (this.first == null)
                throw new RuntimeException("Given graph does not have nodes. Graph:" + this);

            List<Set<Vertex>> sccList = new LinkedList<>();
            Stack<Vertex> stack = this.getDfs();
            Graph reversedGraph = this.getReversedGraph();

            while (!stack.empty()) {
                Vertex current = stack.pop();
                if (!getVertexById(reversedGraph, current.id).isVisited()) {
                    Stack<Vertex> stronglyConnectedElementsStack = new Stack<>();
                    reversedGraph.fillDfsStack(
                            stronglyConnectedElementsStack,
                            getVertexById(reversedGraph, current.id));
                    sccList.add(getSetFromStack(stronglyConnectedElementsStack));
                }
            }
//            Instant finish = Instant.now();
//            long timeElapsed = Duration.between(start, finish).toMillis();
//            System.out.println(timeElapsed + "ms");
            return sccList;
        }

        /**
         * Makes depth-first search on graph.
         *
         * @return stack    {@code Stack<Vertex>} stack of vertices filled during dfs.
         */
        public Stack<Vertex> getDfs() {
            Stack<Vertex> stack = new Stack<>();
            Vertex current = this.first;
            while (current != null) {
                if (!current.isVisited()) fillDfsStack(stack, current);
                current = current.next;
            }
            return stack;
        }

        /**
         * Makes graph reversed.
         *
         * @return graph    {@code Graph} graph with all arcs reversed.
         */
        public Graph getReversedGraph() {
            Graph graph = new Graph(this.id, null);
            duplicateVerticesToGraph(this, graph);
            reverseArcs(this, graph);
            return graph;
        }

        /**
         * Recursive method, fills stack with vertices.
         * <p>
         * Travels throw given vertex arcs and fills stack in correct order on backtrack.
         *
         * @param stack   {@code Stack<Vertex>} stack to fill. Have to be created outside recursion.
         * @param current {@code Vertex} start point of dfs.
         */
        private void fillDfsStack(Stack<Vertex> stack, Vertex current) {
            current.setVisited(true);
            while (current.hasUnvisitedConnections()) {
                Arc nextConnection = current.getUnvisitedConnections();
                fillDfsStack(stack, nextConnection.target);
            }
            stack.add(current);
        }

        /**
         * Creates reversed arcs based on original graphs structure.
         *
         * @param original {@code Graph} original graph
         * @param reversed {@code Graph} graph where reversed arcs will be created.
         */
        private void reverseArcs(Graph original, Graph reversed) {
            if (original.first == null)
                throw new RuntimeException("Given graph does not have nodes. Graph:" + reversed);
            Vertex currentVertex = original.first;
            while (currentVertex != null) {
                Arc currentArc = currentVertex.first;
                while (currentArc != null) {
                    createReverseConnection(reversed, currentArc.target.id, currentVertex.id);
                    currentArc = currentArc.next;
                }
                currentVertex = currentVertex.next;
            }
        }

        /**
         * Creates new reversed connection to graph.
         * <p>
         * NB! Method does not create new vertexes.
         * It finds vertices in current graph.
         * If vertex with given id is not present in graph,
         * the exception will be thrown.
         *
         * @param graph   {@code Graph} given graph where connection will be created.
         * @param from_id {@code String} id of Vertex from where connection will be created.
         * @param to_id   {@code String} id of Vertex where connection will be created.
         */
        private void createReverseConnection(Graph graph, String from_id, String to_id) {
            Vertex parent = getVertexById(graph, from_id);
            Vertex target = getVertexById(graph, to_id);
            if (parent.first == null)
                parent.first = new Arc("a" + from_id + "_" + to_id, target, null);
            else {
                parent.first.getLastConnection().next = new Arc("a" + from_id + "_" + to_id, target, null);
            }
        }

        /**
         * Creates duplicate into duplicated graph based on original graph.
         *
         * @param original   {@code Graph} original graph
         * @param duplicated {@code Graph} duplicated graph where Vertices will be created.
         */
        private void duplicateVerticesToGraph(Graph original, Graph duplicated) {
            Vertex current = original.first;
            if (current == null) return;

            duplicated.first = new Vertex(current.id);
            Vertex next = duplicated.first;

            while (current.hasNextNode()) {
                current = current.next;
                next.next = new Vertex(current.id);
                next = next.next;

            }
        }

        /**
         * Finds vertex by id in current graph.
         * <p>
         * If vertex with given id is not present in current graph,
         * exception will be thrown.
         *
         * @param graph {@code Graph} given graph where to search.
         * @param id    {@code String} id of searched vertex.
         * @return {@code Vertex} vertex of find vertex.
         */
        private Vertex getVertexById(Graph graph, String id) {
            validateIfVertexIsConnectedToGraph(graph, id);
            Vertex current = graph.first;
            while (current != null) {
                if (current.id.equals(id)) return current;
                current = current.next;
            }
            throw new RuntimeException("" +
                    "Current Graph does not contain given vertex." +
                    " Graph: " + graph +
                    " Vertex: " + id
            );
        }

        /**
         * Validates if vertex with given id is present in given graph.
         *
         * @param graph {@code Graph} graph where search will be made.
         * @param id    {@code String} id of vertex which will be searched.
         * @throws RuntimeException if first graph does not have first vertex or
         *                          searchable vertex is not found.
         */
        private void validateIfVertexIsConnectedToGraph(Graph graph, String id) {
            Vertex current = graph.first;
            if (current == null)
                throw new RuntimeException("Graph does not have any Vertices. Check graph first vertex.");

            while (current != null) {
                if (current.id.equals(id)) return;
                current = current.next;
            }
            throw new RuntimeException("" +
                    "Graph does not have Vertex with given id." +
                    "Make sure to connect Vertex to Graph with method: CreateVertex() or " +
                    "set it manually to previous Vertex. (vertex.setNextVertex())" +
                    " Id:" + id);
        }

        /**
         * Creates string representation with directions of given stack.
         *
         * @param stack {@code Stack<Vertex>} stack with vertices.
         * @return {@code String} representation of stack with directions.
         */
        private String getStackRepresentationAsString(Stack<Vertex> stack) {
            StringBuilder stringBuilder = new StringBuilder();
            while (!stack.empty()) {
                stringBuilder.append(stack.pop());
                if (stack.size() != 0)
                    stringBuilder.append(" -> ");
            }
            return stringBuilder.toString();
        }

        /**
         * Reverses stack order.
         *
         * @param vertices {@code Stack<Vertex>} given stack with vertices.
         * @return {@code Stack<Vertex>} stack with reversed order of vertices.
         */
        private Stack<Vertex> reverseStack(Stack<Vertex> vertices) {
            Stack<Vertex> stack = new Stack<>();
            while (!vertices.empty()) {
                stack.add(vertices.pop());
            }
            return stack;
        }

        /**
         * Creates set from stack
         *
         * @param vertices stack with vertices
         * @return created set
         */
        private Set<Vertex> getSetFromStack(Stack<Vertex> vertices) {
            Set<Vertex> set = new HashSet<>();
            while (!vertices.empty()) {
                set.add(vertices.pop());
            }
            return set;
        }
        /**
         * Creates a connected oriented random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree ( int n ) {
            if ( n <= 0 )
                return;

            Vertex[] varray = new Vertex[n];
//            varray[ 0 ] = createVertex("v" + ( 0 ));

            for (int i = 0; i<n; i++) {
                varray[ i ] = createVertex("v" + ( i ));
                if(i>0){
                int vnr = (int) (Math.random()*i);
                createArc("a" + varray[ vnr ].toString() + "_"
                        + varray[ i ].toString(), varray[ vnr ], varray[ i ]);
                }
            }

        }



        /**
         * Creates an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix () {
            int info = 0;
            Vertex v = first;
            while ( v != null ) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[ info ][ info ];
            v = first;
            while ( v != null ) {
                int i = v.info;
                Arc a = v.first;
                while ( a != null ) {
                    int j = a.target.info;
                    res[ i ][ j ]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (directed, no loops, no multiple
         * arcs) random graph with n vertices and edges edges.
         *
         * @param n number of vertices
         */
        public void createRandomSimpleGraph ( int n ) {
            if ( n <= 0 )
                return;

            first = null;
            createRandomTree(n); // n-1 edges created here

            Vertex[] vert = new Vertex[ n ];
            Vertex v = first;
            int c = 0;
            while ( v != null ) {
                vert[ c++ ] = v;
                v = v.next;
            }

            int[][] connected = createAdjMatrix();
            for (int dest = 0; dest < n; dest++) {
                for (int target = n - 1; target > 0; target--) {
                    if ( dest == target )
                        continue;  // no loops
                    if ( connected[ target ][ dest ] != 0 || connected[ dest ][ target ] != 0 )
                        continue;  // no multiple edges
                    int r = (int) (Math.random()*2);
                    Vertex vi = vert[ dest ];
                    Vertex vj = vert[ target ];
                    if(r==0){
                    createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                    connected[ target ][ dest ] = 1;
                    }
                    if(r==1){
                    createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                    connected[ dest ][ target ] = 1;
                    }
                }
            }
        }


        /**
         * Creates indegrees by looping through all the vertices and discovering how many indegrees each vertice has
         *
         * @return an int array with the inDegrees of current graph's vertices
         */
        private Vertex[] createInDegrees () {
            Vertex[] inDegrees = new Vertex[ vertexCount ];
            int counter = 0;
            Vertex v = first;
            while ( v != null ) {
                inDegrees[ counter ] = v;
                v.count = counter;
                counter++;
                v = v.next;
            }
            return inDegrees;
        }

        /**
         * <p>Uses Kahn's-based algorithm to topologically sort the graph by keeping track of vertices indegrees and adding them to the order list. Linear ordering whereas each arc between u(destination) v(target) vertex u come before v
         *
         * @return Topologically ordered array of vertices of this graph only if this graph is oriented and acyclic else returns null.
         * @author William Fiset(Original author)
         * @author Enricqe Rene Sissask(Modified and optimized)
         * <p>Availability: https://github.com/williamfiset/Algorithms/blob/master/src/main/java/com/williamfiset/algorithms/graphtheory/Kahns.java
         */
        public Vertex[] topologicalSortKahn () {

            Vertex[] inDegree = createInDegrees();
            // q hold vertices  where each vertice doesn't have an Arc's target.
            Queue<Vertex> q = new LinkedList<>();
            for (Vertex vertex : inDegree) {
                if ( vertex.inDegrees == 0 ) {
                    q.offer(vertex);
                }
            }
            int index = 0;
            Vertex[] order = new Vertex[ vertexCount ];
            while ( !q.isEmpty() ) {
                Vertex at = q.poll();
                order[ index++ ] = at;
                for (Arc v = at.first; v != null; v = v.next) {
                    v.target.inDegrees--;
                    if ( v.target.inDegrees == 0 ) {
                        q.offer(v.target);
                    }
                }
            }
            return index == vertexCount ? order : null;
        }


        /**
         * Visits each vertice and marks them as visited
         *
         * @return topological ordering of Vertices
         * @author Sesh Venugopal(Original author)
         * @author Enricqe Rene Sissask(Modified and optimized)
         * <p>Availability: https://www.dropbox.com/s/71fe4ttpj27jxa8/Graph.java?dl=0
         */
        public Vertex[] dfsTopsort () {
            if(isContainingCycles()){
                return null;
            }
            boolean[] visited = new boolean[ vertexCount ];
            Vertex[] topnum = new Vertex[ vertexCount ];
            Vertex[] vertexes = createInDegrees();
            int n = vertexCount - 1;
            for (int v = 0; v < visited.length; v++) {
                if ( !visited[ v ] ) {
                    n = dfsTopsort(v, visited, topnum, n, vertexes);
                }
            }
            return topnum;
        }

        /**
         * <p> Recursive dfs based topsort
         *
         * @param n       current largest topological number
         * @param topnum  initially ordered array of vertices
         * @param visited indexes of currently visited vertices
         * @param v       vertices topological number
         * @return Topologically ordered array of vertices of this graph.
         * @author Sesh Venugopal(Original author)
         * @author Enricqe Rene Sissask(Modified and optimized)
         * <p>Availability: https://www.dropbox.com/s/71fe4ttpj27jxa8/Graph.java?dl=0
         */
        private int dfsTopsort ( int v, boolean[] visited, Vertex[] topnum, int n, Vertex[] vertexes ) {
            visited[ v ] = true;

            Arc firstArc = vertexes[ v ].first;
            for (; firstArc != null; firstArc = firstArc.next) {
                if ( !visited[ firstArc.target.count ] ) {
                    n = dfsTopsort(firstArc.target.count, visited, topnum, n, vertexes);
                }
            }

            topnum[ n ] = vertexes[ v ];  // when vertex is going back to previous vertex we assign it as topnum
            return n - 1;  // return to caller
        }

        /**
         * Start looping over all vertices in graph giving one iteration at a time to the findPath method to use it as
         * a starting vertex. reset() is used to clear every true flag in the graph system.
         *
         * @return boolean true/false meaning path was found/notfound.
         */
        public boolean findHamiltonianPath() {
            if (first == null) {
                throw new RuntimeException("Cannot find Hamiltonian path in an empty Graph!");
            }
            Vertex[] vertices = new Vertex[vertexCount];
            Vertex v = first;
            int c = 0;
            boolean hPath;
            while (v != null) {
                vertices[c++] = v;
                v = v.next;
            }
            for (Vertex vert : vertices) {
                hPath = findPath(vert);
                if (hPath) {
                    return true;
                }
                reset(); // Clear checked flags before starting search again.
            }
            return false;
        }

        /**
         * This method clears every true checked flag to false, freeing objects up to be used again in the findPath()
         * method.
         */
        private void reset() {
            Vertex[] vertices = new Vertex[vertexCount];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                v.checked = false;
                v.visited = false;
                vertices[c++] = v;
                v = v.next;
            }
            for (Vertex ver : vertices) {
                if (ver.first == null) {
                    continue;
                }
                ver.first.checked = false;
                if (ver.first.next != null) {
                    Arc nextArc = ver.first.next;
                    nextArc.checked = false;
                    while (nextArc.next != null) {
                        nextArc = nextArc.next;
                        nextArc.checked = false;
                    }
                }
            }

        }


        /**
         * Algorithm to find a Hamilton path in the current graph. Using DFS.
         *
         * @param start Vertex object used as a starting point to start traversing the "Maze" depth first.
         * @return boolean true/false letting the calling method to know whether to continue or stop.
         */
        private boolean findPath(Vertex start) {
            Stack<Vertex> vertStack = new Stack<>();
            List<Arc> path = new ArrayList<>();
            vertStack.push(start);
            Vertex vertex;
            Arc currentArc;
            Arc nextArc;
            int vCount = 1;
            while (!vertStack.isEmpty()) { // while stack has vertices.
                vertex = vertStack.lastElement();
                vertex.checked = true; // check vertex flag to true.
                currentArc = vertex.first; // get vertex first arc
                if (vertStack.size() == vertexCount) { // if number of total vertices in stack then path found
                    vCount = vertexCount;
                    break;
                }
                if (currentArc == null) { // if there is no outgoing arcs then clear vertex flag and pop() it
                    vertex.checked = false;
                    vertStack.pop();
                    continue; // skip other checks
                }
                if (!currentArc.checked) { // if first arc is not flagged then use it
                    if (currentArc.target.checked) { // if current arcs target is flagged true free this vertex
                        currentArc.checked = true; //  flag arc as checked
                        vertex.checked = false;
                        continue; // skip other checks
                    }
                    if(!path.contains(currentArc)) path.add(currentArc); ////
                    vertStack.push(currentArc.target); // else push target to the stack and flag current arc
                    currentArc.checked = true;
                    continue; // skip other checks
                }
                path.remove(currentArc); ////
                if (currentArc.next != null) { // if first arc is already checked, check if there are others.
                    nextArc = currentArc.next; // get next arc
                    if (nextArc.checked) { // if next arc from first is already checked
                        while (nextArc.next != null) { // loop until non flagged arc is found
                            if (!nextArc.next.checked) {
                                path.remove(nextArc); ////
                                nextArc = nextArc.next;
                                break;
                            }
                            path.remove(nextArc); ////
                            nextArc = nextArc.next;
                        }
                        if (nextArc.checked) { // if no unflagged arcs are found then clear everything
                            path.remove(currentArc); ////
                            path.remove(nextArc); ////
                            currentArc.checked = false;
                            nextArc = currentArc.next;
                            nextArc.checked = false;
                            path.remove(nextArc); ////
                            while (nextArc.next != null) {
                                nextArc = nextArc.next;
                                path.remove(nextArc); ////
                                nextArc.checked = false;
                            }
                            vertex.checked = false;

                            vertStack.pop();
                            continue; // skip other checks

                        }
                    }
                    nextArc.checked = true;
                    if(!path.contains(nextArc)) path.add(nextArc); ////
                    if (!nextArc.target.checked) { // if the target is not flagged true then add it to the stack.
                        vertStack.push(nextArc.target);
                        continue;
                    }
                }

                currentArc.checked = false; // dead end clear current vertex and its first arc

                vertex.checked = false;
                vertStack.pop();
            }
            if (vCount == vertexCount) {
                System.out.println("Vertices in Hamilton path in order: " + vertStack + ";");
                return true;
            }
            path.clear();
            return false;
        }

    }
}


